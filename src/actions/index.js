export const setSelectedCard=(card_obj)=>{
    return {
        type: 'UPDATE_SELECTED_CARD',
        obj:card_obj        
    };
}

export const setCurrentUser=(user_obj)=>{
    return {
        type: 'UPDATE_CURRENT_USER',
        obj: user_obj
    }
}

export const updateMessagesList=(chat_obj)=>{
    return {
        type: 'UPDATE_MESSAGES_LIST',
        obj: chat_obj
    }
}