import React, { Component } from 'react';
import './lib/Semantic-UI-CSS-master/semantic.min.css';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import globalReducer from './reducers';
import Header from './components/Header/Header';
import LoginContainer from './components/LoginContainer/LoginContainer';
import HomeContainer from './components/HomeContainer/HomeContainer';
import CardListContainer from './components/CardListContainer/CardListContainer';
import CardInfoContainer from './components/CardInfoContainer/CardInfoContainer';
import GameContainer from './components/GameContainer/GameContainer';
import LiveChat from './components/LiveChat/LiveChat';

const store = createStore(globalReducer);

//extends the object Component
class Main extends Component {
    //class constructor whith given properties
  constructor(props) {
      super(props);
      //if(!this.state.currentContent){ Essayer de ne pas charger home à chaque actualisation mais la page courante
        this.state = {
          currentContent:'LOGIN'
        }
      //}
      this.navigate=this.navigate.bind(this); // cette ligne permet d'assurer que le contexte this utilisé dans les méthodes qui suivent soit bien celui du Main
  }

  navigate(type){
    this.setState({currentContent:type});
  }
    
  //render function use to update the virtual dom
  render() {
    let contentType;    
    
    switch (this.state.currentContent) {
      case 'LOGIN':
        contentType = (
          <LoginContainer navigateMethod={this.navigate}></LoginContainer>          
        );
        break;
      case 'HOME':
        contentType = (
          <HomeContainer navigateMethod={this.navigate}></HomeContainer>
        );
        break;
      case 'BUY':
        contentType = (
          <div className="ui grid">
            <div className="eight wide column">
              <CardListContainer navigateMethod={this.navigate} content='BUY'></CardListContainer>
            </div>
            <div className="eight wide column">
              <CardInfoContainer content='BUY'></CardInfoContainer>
            </div>            
          </div>
        );
        break;
      case 'SELL':
        contentType = (
          <div className="ui grid">
            <div className="eight wide column">
              <CardListContainer navigateMethod={this.navigate} content='SELL'></CardListContainer>
            </div>
            <div className="eight wide column">
              <CardInfoContainer content='SELL'></CardInfoContainer>
            </div>            
          </div>
        );
        break;      
      case 'PLAY':
        contentType = (
          <GameContainer navigateMethod={this.navigate} ></GameContainer>
        );
        break;
      default:
    }

    return (
    <Provider store={store} > 
      <div style={{margin: '2em'}} >
        <div className="row">
            <Header></Header>
        </div>
        <div className="row">
            {contentType}
        </div>
        <div>
        <LiveChat></LiveChat>
        </div>
      </div>
    </Provider>
    );
  }
}

export default Main;