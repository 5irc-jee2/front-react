import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateMessagesList } from '../../actions/index.js';
import io from "socket.io-client";
//importer singleton socket.js et y utiliser une fonction callbabk pour y etre 
//informé d'un nouveau message et, faire remonter l'info au reducer depuis ce component


class LiveChat extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);
        this.socket = io.connect('http://localhost:8080');

        this.state = {
          username: '',
          recipient: '',
          message: '',
          messages: [],
          listUsers: [],
          socket: '',
        };

        this.addMessage=this.addMessage.bind(this);

        this.socket.on('updatechat', this.addMessage);

    }

    addMessage(data) {
      console.log(data);
      this.setState({messages: [...this.state.messages, data.username + ": " + data.message]});
    };

    /*handleOnNewMessage(message){
      this.props.dispatch(updateMessagesList(message));   
    }*/   
    
  //render function use to update the virtual dom
  render() {
    
    return (
        <div style={{position:'absolute', top:'90px', right:'10px', border:'2px solid lightGrey',padding:'10px'}}>
        <b>Live Chat</b><br/>
        <hr/>
        <span><b>Personne 1 : </b></span>
        <span>
        {this.state.messages.map(message => {
          return (
          <div>{message}</div>
          )})
        }
        </span><br/><br/>
        <span style={{float:'right'}}>Votre message<b> : Vous</b></span><br/><br/>
        <span><b>Personne 2 : </b>Message 2</span><br/>
        </div>        
    );
  }
}

/*const mapStateToProps = (state, ownProps) => {
  return {
    chat: state.chatReducer,
  }
};*/

export default connect()(LiveChat);