import React, { Component } from 'react';
import { connect } from 'react-redux';
import MenuButton from '../MenuButton/MenuButton';
import Card from '../Card/Card.js';

class CardListContainer extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);

        this.state = {
            card_list: null
        };    
    }

    getAllCards(){
      fetch('http://localhost:8082/cards')
      .then(res => res.json())
      .then((data) => {
        if((data == undefined) || (data == null)){
          return;        
        } else {
          this.setState({card_list: data.cards});
        }
      })
      .catch(console.log)
    } 

    
    getAllCardRender(){
        let array_render=[];
        if(this.state.card_list != null && this.state.card_list != undefined) {
          switch(this.props.content){
            case 'SELL':
                for(var i=0;i<this.state.card_list.length;i++){
                    if(this.props.currentUser.cardList.filter(id => id==this.state.card_list[i].id).length>0){
                      array_render.push(
                          <Card key={this.state.card_list[i].id} type='SHORT' card={this.state.card_list[i]}></Card>
                          );
                    }
                }
                break;
            case 'BUY':
                for(var i=0;i<this.state.card_list.length;i++){
                    if(this.props.currentUser.cardList.filter(id => id==this.state.card_list[i].id).length==0){
                      array_render.push(
                          <Card key={this.state.card_list[i].id} type='SHORT' card={this.state.card_list[i]}></Card>
                          );
                    }
                }
                break;
            default:
          }
        }        
        return array_render;
    }    
    
  //render function use to update the virtual dom
  render() {
    this.getAllCards();    
    let texte;
    switch(this.props.content){
        case 'BUY':
            texte = ('Cards to buy');
            break;
        case 'SELL':
            texte = ('My card list');
            break;
        default:
    }

    const display_list= this.getAllCardRender();

    return (
      <div class="ui grid">
        <div class="ten wide column">
          <h3 class="ui center aligned header"> {texte}</h3>
             <h3 class="ui aligned header"> {texte}</h3>
            <table class="ui selectable celled table" id="cardListId">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Family</th>
                        <th>HP</th>
                        <th>Energy</th>
                        <th>Defence</th>
                        <th>Attack</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody style={{cursor:'pointer'}}>
                  {display_list}
                </tbody>
            </table>
        </div>
        <div class=" five wide column">
            <div id="card"></div>
        </div>
        <MenuButton navigateMethod={this.props.navigateMethod} value="HOME" imgSrc="http://cdn.onlinewebfonts.com/svg/img_66241.png">
        </MenuButton> 
    </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
    return {
      currentUser: state.userReducer
    }
  };

export default connect(mapStateToProps)(CardListContainer);