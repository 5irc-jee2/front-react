import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as jsonSource from '../../fakeData/fakedata.json';
import { setCurrentUser } from '../../actions/index.js';

document.onkeydown = function (e) {
      e = e || window.event;
      switch (e.which || e.keyCode) {
            case 13 : //Your Code Here (13 is ascii code for 'ENTER')
                document.getElementById('loginBtn').click();
                break;
      }
    }

class LoginContainer extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);

        let temp_card_list=jsonSource.default;

        this.state={
          username:"",
          password:"",
          userList: temp_card_list.users,
          requestUser: null
        }

        this.handleOnUserChange=this.handleOnUserChange.bind(this);
    }

    handleOnUserChange(user_obj){
      this.props.dispatch(setCurrentUser(user_obj));   
    }

    getUserByLogin(login,password){
        fetch('http://localhost:8080/auth?login='+login+'&pwd='+password)
        .then(res => res.json())
        .then((data) => {
          console.log(data);
          this.requestUser=data;
          if((this.requestUser == undefined) || (this.requestUser == null)){
            this.props.navigateMethod('LOGIN');
            return;        
          } else {
            this.handleOnUserChange(this.requestUser);
            this.props.navigateMethod('HOME');
            return;
          }
        })
        .catch(console.log)
    }    

    onChangeUser(ev){
      let tempUser=ev.target.value;
      this.setState({username:tempUser});
    }

    onChangePassword(ev){
      let tempPwd=ev.target.value;
      this.setState({password:tempPwd});
    }    
    
  //render function use to update the virtual dom
  render() {
    return (
      <form className="ui form" style={{position: 'absolute'}} style={{marginLeft: '44%'}}>
        <div className="field">
          <label>Username</label>
          <input type="text" name="username" placeholder="JeanMiDu13" style={{width:'auto'}} value={this.state.username} onChange={(ev)=> this.onChangeUser(ev)}/>
        </div>
        <div className="field">
          <label>Password</label>
          <input type="password" name="pwd" placeholder="" style={{width:'auto'}} value={this.state.password} onChange={(ev)=> this.onChangePassword(ev)}/>
        </div>
        <button id="loginBtn" className="ui primary button" type="button" onClick={()=>this.getUserByLogin(this.state.username,this.state.password)}>Login</button>
        <button className="ui button" type="button">Cancel</button>
      </form>
    );
  }
}

export default connect()(LoginContainer);