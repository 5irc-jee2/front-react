import React, { Component } from 'react';
import { connect } from 'react-redux';
//import MenuButton from '../MenuButton/MenuButton';
import ShortCard from './visuals/ShortCard.js';
import FullCard from './visuals/FullCard.js';

class Card extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);
    }
    
  //render function use to update the virtual dom
  render() {
      let render;
      switch(this.props.type){
        case 'FULL':
            return (
                <FullCard card={this.props.card} buysellmethod={this.props.buysellmethod}></FullCard>
            );
        case 'SHORT':
            return (
                <ShortCard card={this.props.card}></ShortCard>
            );
        default:
            return (
                <div className="ui segment">
                    Le type d'affichage n'a pas été reconnu
                </div>        
            );
    }   
  }
}

export default connect()(Card);