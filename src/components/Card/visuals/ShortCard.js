import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setSelectedCard } from '../../../actions';

class ShortCard extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);

        this.handleOnCardSelected=this.handleOnCardSelected.bind(this);
    }

    handleOnCardSelected(card_obj){
        this.props.dispatch(setSelectedCard(card_obj));   
    }
    
  //render function use to update the virtual dom
  render() {
    return (
        <tr onClick={()=>{this.handleOnCardSelected(this.props.card)}}>
            <td>
                <img  class="ui avatar image" src="https://vignette.wikia.nocookie.net/lego/images/4/48/76096_Minifigure_04.jpg/revision/latest/scale-to-width-down/250?cb=20190729133554"/> <span>{this.props.card.name} </span>
            </td>
            <td>{this.props.card.description}</td>
            <td>{this.props.card.family}</td>
            <td>{this.props.card.hp}</td>
            <td>{this.props.card.energy}</td>
            <td>{this.props.card.defense}</td>
            <td>{this.props.card.attack}</td>
            <td>{this.props.card.price}$</td>
        </tr>              
    );
  }
}

export default connect()(ShortCard);