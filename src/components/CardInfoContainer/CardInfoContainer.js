import React, { Component } from 'react';
import { connect } from 'react-redux';
//import MenuButton from '../MenuButton/MenuButton';
import Card from '../Card/Card.js';
import { setCurrentUser, setSelectedCard } from '../../actions/index.js';

class CardInfoContainer extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);
        this.state = {
        };

        this.buyCard=this.buyCard.bind(this);
        this.sellCard=this.sellCard.bind(this);
    }

    buyCard(card){
      fetch('http://localhost:8081/buy', {method: "POST"})
        .then(res => res.json())
        .then((data) => {
          console.log(data);
          if(data != null && data != undefined){            
            this.props.dispatch(setCurrentUser(data)); 
            this.props.dispatch(setSelectedCard(null));
          }
        })
        .catch(console.log);
    }

    sellCard(card){
      fetch('http://localhost:8081/sell', {method: "POST"})
      .then(res => res.json())
      .then((data) => {
        console.log(data);
        if(data != null && data != undefined){            
          this.props.dispatch(setCurrentUser(data)); 
          this.props.dispatch(setSelectedCard(null));
        }
      })
      .catch(console.log);
    }
    
  //render function use to update the virtual dom
  render() {
    let buysell;
    switch(this.props.content){
      case 'BUY':
        buysell = this.buyCard;
        break;
      case 'SELL':
        buysell = this.sellCard;
        break;
      default:
    }
    return (        
        <Card card={this.props.card} type='FULL' buysellmethod={buysell}></Card>      
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    card: state.cardReducer,
    currentUser: state.userReducer,
  }
};

export default connect(mapStateToProps)(CardInfoContainer);