import React, { Component } from 'react';
import { connect } from 'react-redux';

class Header extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);        
    }
    
  //render function use to update the virtual dom
  render() {
      let login,money;
      if(this.props.currentUser != null){
        login = this.props.currentUser.login;
        money = this.props.currentUser.money;
      } else {
          login = 'No user connected';
          money = '';
      }
    return (
        <div className="ui secondary  menu">
            <span className="item">
               <b>{money}</b>
            </span>
            <div style={{marginLeft: '40%'}}>
            <h1>
                Yu-Gi-Oh! Online
            </h1>
            </div>
            <div class="right menu">
                <a class="item">
                    <img src={'https://www.stickpng.com/assets/images/585e4bf3cb11b227491c339a.png'}></img>
                </a>
                <a class="item">
                <b>{login}</b>
                </a>
            </div>
        </div>
    );
  }
}


const mapStateToProps = (state, ownProps) => {
    return {
      currentUser: state.userReducer
    }
  };

//export the current classes in order to be used outside
export default connect(mapStateToProps)(Header);
