import React, { Component } from 'react';
import { connect } from 'react-redux';
import MenuButton from '../MenuButton/MenuButton';

class GameContainer extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);
    }
    
  //render function use to update the virtual dom
  render() {
    return (
        <div className="ui special cards" >
            <div className="card">
                <div className="content">
                    <div className="ui grid">
                        <div className="three column row">
                            <div className="column" >
                                <a className="ui red circular label">10</a>
                            </div>
                            <div className="column" >
                                <h5>Name</h5>
                            </div>
                            <div className="column" >
                                <a className="ui yellow circular label">10</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="image imageCard">
                    <div className="ui fluid image">
                        <img id="cardImgId" className="ui centered image"/>
                    </div>
                </div>
            </div>
            <MenuButton navigateMethod={this.props.navigateMethod} value="HOME" imgSrc="http://cdn.onlinewebfonts.com/svg/img_66241.png">
            </MenuButton>                                    
        </div>      
    );
  }
}

export default connect()(GameContainer);