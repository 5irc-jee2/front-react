import React, { Component } from 'react';
import { connect } from 'react-redux';
import MenuButton from '../MenuButton/MenuButton';

class HomeContainer extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);
    }
    
  //render function use to update the virtual dom
  render() {
    return (
      <div className="ui grid" style={{position: 'absolute'}} style={{marginLeft: '35%'}}>
        <div className="three wide column">
          <MenuButton navigateMethod={this.props.navigateMethod} value="BUY" imgSrc="https://image.flaticon.com/icons/png/512/126/126083.png">
          </MenuButton>
        </div>
        <div className="three wide column">
          <MenuButton navigateMethod={this.props.navigateMethod} value="SELL" imgSrc="https://image.flaticon.com/icons/png/512/69/69881.png">
          </MenuButton>
        </div>
        <div className="three wide column">
          <MenuButton navigateMethod={this.props.navigateMethod} value="PLAY" imgSrc="https://icon-library.net/images/game-controller-icon/game-controller-icon-0.jpg">
          </MenuButton>
        </div>
      </div>
    );
  }
}

export default connect()(HomeContainer);