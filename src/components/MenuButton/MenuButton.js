import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setSelectedCard } from '../../actions/index.js';

class MenuButton extends Component {
    //class constructor whith given properties
    constructor(props) {
        super(props);
    }
    
  //render function use to update the virtual dom
  render() {
    return (
            <button class="ui button" onClick={()=>{              
              this.props.dispatch(setSelectedCard(null));
              this.props.navigateMethod(this.props.value);
            }}>
            <img src={this.props.imgSrc} style={{height:'1em'}}></img>
            &nbsp; {this.props.value}
            </button>
    );
  }
}

export default connect()(MenuButton);