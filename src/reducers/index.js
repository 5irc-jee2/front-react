import { combineReducers } from 'redux';
import cardReducer from './cardReducer';
import userReducer from './userReducer';
import chatReducer from './chatReducer';

const globalReducer = combineReducers({
    cardReducer: cardReducer,
    userReducer: userReducer,
    chatReducer: chatReducer
});

export default globalReducer;